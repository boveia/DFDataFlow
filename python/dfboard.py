import utils
import copy
import logging
from sets import Set

class Board:
	def __init__(self, in_board_number, t1 = None, t2 = None):
		self.board_number = in_board_number
		# ROB channels from the IM
		self.im_lanes = [0]*16
		#for lane, ROBID in laneMap.iteritems():
		#	self.im_lanes[lane] = ROBID
		self.tower1 = t1
		self.tower2 = t2

		self.tower1_robids = {}
		self.tower2_robids = {}

		# all other boards in the shelf
		self.shelfboardlist = []

		# board with direct fiber connection
		self.fiberboard = None

		# map of robid -> board sequence
		self.inputs = {}
		self.outputs = {}
		self.reset()
		
		self.traceModule = None
		
		# keep track of data flow
		self.words_received_im = 0
		self.words_received_fabric = 0
		self.words_received_fiber = 0
		#self.words_sent_tower = 0
		self.words_sent_ssb = 0
		self.words_sent_aux = 0
		self.words_sent_fabric = 0
		self.words_sent_fiber = 0
		self.modules_received_im = 0
		self.modules_received_fabric = 0
		self.modules_received_fiber = 0
		#self.modules_sent_tower = 0
		self.modules_sent_ssb = 0
		self.modules_sent_aux = 0
		self.modules_sent_fabric = 0
		self.modules_sent_fiber = 0
		
	# expect tuple i the form of (robid, moduleid)
	# print out extra info for this module
	def setTraceModule(self, trace):
		self.traceModule = trace
		
	def tracing(self, module):
		if not self.traceModule: return False
		return module.robid == self.traceModule[0] and module.id == self.traceModule[1] and module.event == self.traceModule[2]

	def setIMLaneROBID(self, lane,ROBID):
		self.im_lanes[lane] = ROBID

	def setShelf(self, in_shelf):
		self.shelf= in_shelf

	def setSlot(self, in_slot):
		self.slot= in_slot

	def setDict(self,in_dict):
		self.boardDict = in_dict
		
	def addInputLink(self, robid, modid, link):
		if robid not in self.inputs: self.inputs[robid] = {}
		try:
			if link not in self.inputs[robid][modid]:
				self.inputs[robid][modid].append(link)
			else: logging.debug("Link in board {0} for module {1} {2} already set: {3}".format(self.board_number,modid, robid, link))
		except:
			self.inputs[robid][modid] = [link]
			
	def addOutputLink(self, robid, modid, link):
		if robid not in self.outputs: self.outputs[robid] = {}
		try:
			if link not in self.outputs[robid][modid]:
				self.outputs[robid][modid].append(link)
			else: logging.debug("Link in board {0} for module {1} {2} already set: {3}".format(self.board_number,modid, robid, link))
		except:
			self.outputs[robid][modid] = [link]

	def tower1WantsModule(self, robid, modid = -1):
		return robid in self.tower1_robids.keys() and (modid == -1 or modid in self.tower1_robids[robid])
		
	def tower2WantsModule(self, robid, modid = -1):
		return robid in self.tower2_robids.keys() and (modid == -1 or modid in self.tower2_robids[robid])
			
	def wantsModule(self, robid, modid):
		return self.tower1WantsModule(robid, modid) or self.tower2WantsModule(robid, modid)

	######### Data Flow functions ###########################

	def reset(self):
		self.ido = [] # IM slink inputs
		self.odo = [] # tower outputs
		self.ili = [] # inter and intrashelf inputs
		self.ilo = [] # inter and intrashelf outputs

	# intrashelf im or fiber
	def receive(self, module, linktype):
	
		# counting dataflow
		if linktype == 'im':
			self.words_received_im     += len(module.words)
			self.modules_received_im += 1
		elif linktype == 'fiber':
			self.words_received_fiber  += len(module.words) 
			self.modules_received_fiber  += 1
		elif linktype == 'intrashelf':
			self.words_received_fabric += len(module.words)
			self.modules_received_fabric += 1
		
	
		if self.tracing(module):
			utils.trace("Board {0} received module id {1} robid {2} from {3}".format(self.board_number, self.traceModule[1], self.traceModule[0],linktype))
			utils.trace("{0}".format(module))

		module.lastlink = linktype
		module.currentboard = self.board_number
		
		if module.robid not in self.im_lanes and module.robid not in self.inputs.keys() and module.robid not in self.outputs.keys():
			utils.error("receive: Module {0} with robid {1} not expected in board {2} from {3} link".format(module.id, module.robid,self.board_number, linktype))
		
		if linktype == 'im':
			module.firstboard = self.board_number
			if len(module.paths) > 0:
				print module, linktype
				for p in module.paths: print p
				utils.error("receive: Module with robid {0} in board {1} already has set paths!!".format(module.robid, self.board_number))

			if module.robid in self.outputs and module.id in self.outputs[module.robid]:
				module.paths += self.outputs[module.robid][module.id]
			if module.robid in self.inputs and module.id in self.inputs[module.robid]:
				module.paths += [path for path in self.inputs[module.robid][module.id] if path.linktype == 'im']

			if len(module.paths) > 0:
				self.ido.append(module)
			#else: utils.warning("receive: Module {0} with robid {1} in board {2} has no set path!".format(module.id, module.robid, self.board_number))
		else:
			self.ili.append(module)
			
		if self.tracing(module):
			utils.trace("Module {1} {0} paths".format(len(module.paths), 'received' if linktype == 'im' else 'has'))
			for p in module.paths: utils.trace("Module Path: {0}".format(p))

	def clearOutputs(self):
		self.odo = []
		self.ilo= []

	def clearInputs(self):
		self.ido = []
		self.ili = []

	def intraBoardDataFlow(self):
		
		# iterate over all modules in the input buffers
		for module in self.ili + self.ido:
		
			if self.tracing(module):
				utils.log("-----------------------------")
				utils.trace("Board {0} doing intraboard dataflow for module {1} robid {2}".format(self.board_number, module.id, module.robid))
				#for p in module.paths:
				#	utils.trace("path to Board {0}: {1}".format(p.endboard, p))
				utils.log("-----------------------------")
				
			sent_module = False
			if len(module.paths) == 0:
				utils.warning("intraBoardDataFlow: Module {0} with robid {1} in board {2} has no set path!".format(module.id, module.robid, self.board_number))
				continue
		
			# send module to board towers
			if  self.tower1 in module.towers or self.tower2 in module.towers:

				# duplicate board to send to towers

				newmodule = copy.deepcopy(module)
				newmodule.paths = [p for p in module.paths if p.endboard == self.board_number]
				# there should be one path to this board's output towers
				if len(newmodule.paths) != 1:
					utils.error("intraBoardDataFlow: Module {0} with robid {1} in board {2} has too many paths!".format(module.id, module.robid, self.board_number))
				module.paths.remove(newmodule.paths[0])

				if newmodule in self.odo:
					logging.warning("intraBoardDataFlow: Module already in output buffer!")
					logging.warning(newmodule)
					logging.warning(len(self.odo))
					
				if self.tracing(module):
					utils.trace("send module to board {0} towers: {1}".format(self.board_number, newmodule))
					utils.trace("module took path: {0}".format(newmodule.paths[0]))
				self.odo.append(newmodule)
				sent_module = True

		# check if module needs to be sent to another board
	
			# consistency check
			if not sent_module :
				if module.robid not in self.outputs.keys() :
					utils.error("intraBoardDataFlow: Module with robid {0} in board {1} and should go to board {2} but no output path exists".format(module.robid, self.board_number, b))

			if module.robid in self.outputs.keys() and module not in self.ilo:
				if self.tracing(module): utils.trace("send module to output buffers for other boards...")
				self.ilo.append(module) 

	def interBoardDataFlow(self):
	
		for m in self.odo:
			if self.planeToOutBitMap[m.plane] == 8:
				self.words_sent_ssb += len(m.words)
				self.modules_sent_ssb += 1
			else:
				self.words_sent_aux += len(m.words)
				self.modules_sent_aux += 1
			#self.words_sent_tower += len(m.words)
			#self.modules_sent_tower += 1
		
		# iterate over all modules in the output buffer
		for module in self.ilo:
			if self.tracing(module):
				utils.log("-----------------------------")
				utils.trace("Board {0} doing interboard dataflow for module {1} robid {2}".format(self.board_number, self.traceModule[1], self.traceModule[0]))
				utils.log("-----------------------------")
			sent_boards = []
			for p in module.paths:
				nextboard = p.nextBoard(self.board_number)
				nextlink  = p.nextLink(self.board_number)
				if nextboard in sent_boards: continue
				sent_boards.append(nextboard)

				if self.tracing(module):
					utils.trace("Found path: {0}".format(p))
					utils.trace( "Sending to board {0} across {1}".format(nextboard,nextlink))
				
				newmodule = copy.deepcopy(module)
				newmodule.fromboard = self.board_number
				newmodule.fromslot  = self.slot
				newmodule.fromshelf = self.shelf
				newmodule.paths = [p for p in module.paths if p.nextBoard(self.board_number) == nextboard]
				newmodule.towers = utils.flatten_list(p.outtower for p in newmodule.paths)

				self.boardDict[nextboard].receive(newmodule, nextlink)
				
				if nextlink == 'intrashelf':
					self.words_sent_fabric += len(module.words)
					self.modules_sent_fabric += 1
				elif nextlink == 'fiber':
					self.words_sent_fiber += len(module.words)
					self.modules_sent_fiber += 1


	######################################################

	def getInfo(self):
		utils.log("####################################################")
		utils.log( "# Summary for DF board {0} in shelf {1} slot {2}".format(self.board_number, self.shelf, self.slot))
		utils.log( "####################################################")
		utils.log("Output to towers {0} and {1}".format(self.tower1,self.tower2))
		if self.fiberboard != None:
			utils.log( "Fiber connection to {0}".format(self.fiberboard.board_number))

		utils.log( "DF{1} IM ROBIDs = {0}".format(self.im_lanes, self.board_number))
		utils.log( "DF{1} Top Tower ({2}) ROBIDS = {0}".format(self.tower1_robids, self.board_number,self.tower1))
		utils.log( "DF{1} Bottom Tower ({2}) ROBIDS = {0}".format(self.tower2_robids, self.board_number,self.tower2))

		for t in self.tower1_missing_inputs:
			utils.log( "missing {0} for Tower {1}".format(t,self.tower2) )
		for t in self.tower2_missing_inputs:
			utils.log( "missing {0} for Tower {1}".format(t,self.tower1) )

		def printLinks(linkmap):
			for robid, modid_map in linkmap.iteritems():
				for modid, links in modid_map.iteritems():
					if len(links) == 0: continue
					utils.log( "{0} {1}\t{2}".format( robid, modid, links[0]))
					if links[0].startboard != self.board_number:
						utils.error("Problem with link!")
					for link in links[1:]:
						utils.log('\t\t{0}'.format(link))
						if link.startboard != self.board_number:
							utils.error("Problem with link!")


		utils.log("ROBID inputs & paths:")
		printLinks(self.inputs)
		utils.log("ROBID outputs & paths:")
		printLinks(self.outputs)

	def wantsROBID(self, checkrobid = None):
		tower1_robids, tower2_robids = self.getDesiredROBIDs()
		if checkrobid in tower1_robids or checkrobid in tower2_robids:
			return True
		return False

	def getDesiredROBIDs(self):
		tower1_robids =  [utils.toHex(r) for r in self.tower1_robids.keys()]
		tower2_robids = [utils.toHex(r) for r in self.tower2_robids.keys()]
		return tower1_robids, tower2_robids


	def printIO(self):
		print 'IDO (IM slink inputs)  :', len(self.ido), 'modules'
		print 'ODO (tower outputs)	:', len(self.odo), 'modules'
		print 'ILI (interboard inputs):', len(self.ili), 'modules'
		print 'ILO (interboard outputs)', len(self.ilo), 'modules'
	def getIO(self):
		return (self.ido, self.odo, self.ili, self.ilo)
	def printTowerOutput(self):
		print self.odo

	def printInputIMLanes(self):
		print 'Input IM Lanes:'
		for i, lane in enumerate(self.im_lanes):
			inputs_in_lane = [m for m in self.ido if m.robid == lane]
			print "Board {0} Lane {1} (ROBID {2}) modules: {3} ".format(self.board_number, i, lane, len(inputs_in_lane))

class Link(object):
	def __init__(self, startboard, endboard, outtower, linktype, steps, middleboards = []):
		self.startboard = startboard
		self.endboard = endboard
		if isinstance(outtower, (list, tuple)):
			self.outtower = outtower
		else:
			self.outtower = [outtower]
		self.linktype = linktype
		self.steps = steps
		self.middleboards = middleboards
		
	def getBoards(self): return [self.startboard] + self.middleboards + [self.endboard]
	def getLinks(self):  return self.linktype

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.__dict__ == other.__dict__
		else:
			return False
	def __ne__(self, other):
		return not self.__eq__(other)
		
	def issubset(other):
		if isinstance(other, self.__class__):
			return Set(self.getBoards()).issubset(Set(other.getBoards()))
		else:
			return False

	def getChainStr(self, direction):
		if self.steps == 1:
			return "{0} {4}({1}){4} {2} to tower: {3}".format(self.startboard, self.linktype, self.endboard,self.outtower,direction)
		else:
			otherboards = self.middleboards + [self.endboard]
			linkstr = "{0}".format(self.startboard)
			for i,lt in enumerate(self.linktype):
				linkstr += " {2}({0}){2} {1}".format(lt,otherboards[i], direction)
			return linkstr + " to tower: {0}".format(self.outtower)

class OutLink(Link):
	def __init__(self, startboard, endboard, outtower, linktype, steps, middleboards = []):
		super(OutLink, self).__init__(startboard, endboard, outtower, linktype, steps, middleboards)
		if steps < 1:
			sys.exit(2)
	def nextBoard(self, current_board_number = None):
		if len(self.middleboards) == 0:
			return self.endboard
		elif current_board_number == self.middleboards[-1]:
			return self.endboard
		elif current_board_number == self.middleboards[0]:
			return self.middleboards[-1]
		else:
			return self.middleboards[0]
	def nextLink(self, current_board_number = None):
		if len(self.middleboards) == 0:
			return self.linktype
		elif current_board_number:
			boards = self.getBoards()
			try:
				index = boards.index(current_board_number)
				return self.linktype[index]
			except:
				utils.error("Cannot find current board {0} in path {1}".format(current_board_number, boards))
		return self.linktype[0]
	def __str__(self):
		return self.getChainStr('->')
	def reverse(self):
		return InLink(self.endboard, self.startboard, self.outtower, self.linktype[::-1], self.steps, self.middleboards[::-1])
		

class InLink(Link):
	def __init__(self, startboard, endboard, outtower, linktype, steps, middleboards = []):
		super(InLink, self).__init__(startboard, endboard, outtower, linktype, steps, middleboards)
		if steps < 1:
			sys.exit(2)
	def previousBoard(self):
		if len(self.middleboards) == 0:
			return self.endboard
		else:
			return self.middleboards[0]
	def previousLink(self):
		if len(self.middleboards) == 0:
			return self.linktype
		return self.linktype[0]
	def __str__(self):
		return self.getChainStr('<-')
	def reverse(self):
		return OutLink(self.endboard, self.startboard, self.outtower, self.linktype[::-1], self.steps, self.middleboards[::-1])
	def split(self, n):
		nindex = self.middleboards.index(n)
		middle2 = self.middleboards[:nindex]
		middle1 = self.middleboards[nindex+1:]

		steps1 = len(middle1)+1
		steps2 = len(middle2)+1

		linktype1 = self.linktype[::-1][:steps1][::-1]
		linktype2 = self.linktype[:steps2][::-1]

		if len(linktype1) == 1: linktype1 = linktype1[0]
		if len(linktype2) == 1: linktype2 = linktype2[0]

		link1 = InLink( n, self.endboard,   self.outtower, linktype1, steps1, middle1)
		link2 = OutLink(n, self.startboard, self.outtower, linktype2, steps2, middle2)
		return(link1, link2)

class IMLink(Link):
	def __init__(self, startboard, endboard, outtower, linktype, steps, middleboards = []):
		super(IMLink, self).__init__(startboard, endboard, outtower, linktype, steps, middleboards)
	def nextBoard(self, current_board_number = None):
			return self.endboard
	def nextLink(self, current_board_number = None):
		return self.linktype
	def __str__(self):
		return "from IM to tower: {0}".format(self.outtower)

class Module:
	def __init__(self, thisid, robid, tower, words = [], layer = -1, event = 0):
		self.id = thisid
		self.robid = robid
		self.tower = tower
		self.layer = layer
		self.words = words
		self.lastlink = 'unknown'
		self.fromboard = -1
		self.currentboard = -1
		self.firstboard = -1
		self.inputimlane = -1
		self.event = event
		self.paths = []
		self.plane = -1
		self.towers = []

		self.isSCT = self.robid >= 0x200000
		self.isPix = not self.isSCT
		if self.isSCT: self.type = "SCT"
		else:		  self.type = "Pixel"


	def addTowers(self, in_towers):
		for t in in_towers:
			if t not in self.towers:
				self.towers.append(t)

	def getPathTowers(self):
		return [p.outtower for p in self.paths]
	
	def __copy__(self):
		copy_object = Module()
		return copy_object

	def __deepcopy__(self, memodict={}):
		copy_object = Module(self.id, self.robid, self.tower, self.words, self.layer, self.event)
		copy_object.lastlink  = self.lastlink 
		copy_object.fromboard = self.fromboard 
		copy_object.currentboard  = self.currentboard 
		copy_object.firstboard  = self.firstboard 
		copy_object.inputimlane = self.inputimlane 
		copy_object.plane = self.plane
		copy_object.towers = self.towers

		return copy_object


	def __str__(self):
		try:
			data = (self.id,
					 self.robid,
					 len(self.words),
					 self.lastlink,
					 self.fromboard,
					 self.plane,
					 str(self.towers))
			return "Module {0}; ROBID {1}; words {2}; came via {3} from df {4} in event {5}; goes to plane {6} towers: {7}".format(self.id,
					 self.robid,
					 len(self.words),
					 self.lastlink,
					 self.fromboard,
					 self.event,
					 self.plane,
					 str(self.towers))
		except:
			return "Module {0}; ROBID {1}; words {2}; came via {3} from df {4} in event {5}; goes to towers: {6}".format(self.id,
					 self.robid,
					 len(self.words),
					 self.lastlink,
					 self.fromboard,
					 self.event,
					 str(self.towers))

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.id == other.id and self.robid == other.robid and self.words == other.words and self.event == other.event 
		else:
			return False

	def __ne__(self, other):
		return not self.__eq__(other)

	def getData(self, doHex = False):
		data = []
		moduleheader = [1,0,0,0]
		moduleheader += [0]*12 # reserved

		if self.isPix:
			moduleheader += [0,0,0,0,0] # first entry for pixel, rest reserved
			moduleid = utils.number_to_bit_list(self.id, 11)
		else:
			moduleheader += [1,0,0] # first entry for pixel, rest reserved
			moduleid = utils.number_to_bit_list(self.id, 13)
		moduleheader += moduleid
		moduleheader = utils.make_str_from_list(moduleheader)
		if doHex:
			#moduleheader = hex(int(moduleheader, 2))
			moduleheader = "{0:#0{1}x}".format(int(moduleheader, 2),10)


		data.append(moduleheader)
		for w in self.words:
			thisword = utils.make_str_from_list(utils.number_to_bit_list(int(w),32))
			if doHex:
				#thisword = hex(int(thisword, 2))
				thisword = "{0:#0{1}x}".format(int(thisword, 2),10)
			data.append(thisword)
		return data


					