from collections import namedtuple
import dfconfigparser
import sys

Module = namedtuple("Module","isPix isSCT barrel_ec layer phi_module eta_module ROBID")

def getInputROBIDS(infilename):
	mfile = open(infilename, 'r')
	myrobids = []
	for line in mfile:
		toks = line.strip().split()
		if len(toks)<3 or (toks[0][0] == '#'):
		  continue

		if toks[0] != "rodToBoard": continue
		if 'none' in line: continue

		robid  = int(toks[1], 16) # ROBId in hex

		myrobids.append(robid)
	return myrobids

# Barrel_EC	Layer_Disk	Phi_module	Eta_module	ROBID	RODID	40FMT	40Link	80FMT	80Link	DCS Geographical ID
def parsePixCablingMap(infilename):
	#print 'parsing', infilename
	file = open(infilename, 'r')
	pixmap = []
	for line in file:
		toks = line.strip().split()
		if len(toks) == 0: continue
		if '#' in toks[0]: continue

		(barrel_ec, layer, phi_module, eta_module, ROBID, RODID, __ , __, __, __, name) = toks
		ROBID = int(ROBID, 16)
		pixmap.append(Module(isPix = True, isSCT = False, barrel_ec = barrel_ec, layer=layer, phi_module = phi_module, eta_module = eta_module, ROBID = ROBID))
	return pixmap

def parseSCTCablingMap(infilename):
	#print 'parsing', infilename
	file = open(infilename, 'r')
	readcsv = infilename[-4:] == '.csv'
	sctmap = []
	doParseSCT = False
	for line in file:
		barrel_ec,  layer, eta_module, phi_module, side, ROBID = None, None, None, None, None, None

		if readcsv:
			if line[0] == '#': continue
			toks = [t for t in line.split('\t')]
			(__, ROBID, __, __, __, __, __, barrel_ec, layer, eta_module, phi_module, __, __) = toks
			ROBID = int(ROBID)
		else:
			if 'textList name="SCT cabling mapping"' in line:
				doParseSCT = True
				continue
			elif '</textList>' in line:
				doParseSCT = False
			if not doParseSCT: continue

			toks = line.strip().split()
			if len(toks) == 0: continue
			(__, __, barrel_ec,  layer, eta_module, phi_module, __, ROBID, __) = toks
			ROBID = int(ROBID, 16)

		sctmap.append(Module(isPix = False, isSCT = True, barrel_ec = barrel_ec, layer=layer, phi_module = phi_module, eta_module = eta_module, ROBID = ROBID))

	return sctmap

#for debugging
def studySCTCablingMap(infilename):
	#print 'parsing', infilename
	file = open(infilename, 'r')
	doParseSCT = False
	layers = [0]*9
	for line in file:
		if 'textList name="SCT cabling mapping"' in line:
			doParseSCT = True
			continue
		elif '</textList>' in line:
			doParseSCT = False
		if not doParseSCT: continue

		toks = line.strip().split()
		if len(toks) == 0: continue
		(__, __, barrel_ec,  layer, eta_module, phi_module, side, ROBID, __) = toks
		if barrel_ec == '0':
			layers[int(layer)]+=1
	print layers

def buildIDHashROBMapfromModuleConfig(modulelist_configfile):
	PixIDHASHtoROBIDmap = {}
	SCTIDHASHtoROBIDmap = {}

	# ROBmap: ROBId -> tower -> [ (modid, layer) ]
	ROBmap = dfconfigparser.parseModuleListFile( open(modulelist_configfile,'r'))

	for robid, towermap in ROBmap.iteritems():
		isSCT = robid >= 0x200000
		for modtower, modlist in towermap.iteritems():
			for (modid, modsector) in modlist:
				if isSCT:
					if modid in SCTIDHASHtoROBIDmap and SCTIDHASHtoROBIDmap[modid] != robid:
						print 'ERROR!'
					else: SCTIDHASHtoROBIDmap[modid] = robid
				else:
					if modid in PixIDHASHtoROBIDmap and PixIDHASHtoROBIDmap[modid] != robid:
						print 'ERROR!'
					else: PixIDHASHtoROBIDmap[modid] = robid
							
	return (PixIDHASHtoROBIDmap, SCTIDHASHtoROBIDmap)



def buildIDHashROBMap(pixmapname, sctmapname, datafilename):
	pixmap = parsePixCablingMap(pixmapname)
	sctmap = parseSCTCablingMap(sctmapname)
	PixIDHASHtoROBIDmap = {}
	SCTIDHASHtoROBIDmap = {}

	# data file should have line structure
	# with the corrected layer and eta module
	# Tower Event isSCT isPixel BarrelEC Layer PhiModule EtaModule PhiSide IDHash
	file = open(datafilename,'r')
	prev_event = -1

	from sets import Set
	all_sct_modules = Set()
	all_pix_modules = Set()
	
	for line in file:
		toks = line.strip().split()
		if len(toks) == 0: continue
		if '#' == toks[0]: continue
		(tower, event, isSCT, isPix, barrel_ec, layer, phi_module, eta_module, phiside, idhash) = toks[:10]
		event = int(event)
		tower = int(tower)
		idhash_int = int(idhash)

		if event != prev_event:
			print event
			prev_event = event
		
		matchedModules = []
		
		if isPix == '1':
			if idhash_int not in all_pix_modules:
				all_pix_modules.add(idhash_int)
			else: continue
			for module in pixmap:
				# check for matching module in pixel map
				if module.barrel_ec == barrel_ec and module.phi_module == phi_module and module.eta_module == eta_module and module.layer == layer:
					matchedModules.append(module)
						
		elif isSCT == '1': 
			if idhash_int not in all_sct_modules:
				all_sct_modules.add(idhash_int)
			else: continue
			for module in sctmap:
				if module.barrel_ec == barrel_ec and module.phi_module == phi_module and module.eta_module == eta_module and module.layer == layer:
					matchedModules.append(module)

		if len(matchedModules) > 1:
			firstROBID = matchedModules[0].ROBID
			for m in matchedModules[1:]:
				if firstROBID != m.ROBID:
					print 'INCONISTENT ROBIDS'

		if len(matchedModules) == 0:
			print 'No matched module found for ',  ("SCT" if isSCT == '1' else 'PIX'), 'module with BEC=', barrel_ec, ' layer=', layer, ' phim=',phi_module, ' etam=',eta_module
			if isSCT == '1': 
				for module in sctmap:
					matching_terms = [module.barrel_ec == barrel_ec,  module.phi_module == phi_module, module.eta_module == eta_module, module.layer == layer]
					if sum(matching_terms) >= 3:
						print matching_terms, module

		if len(matchedModules) > 0:		
			if isPix == '1':
				if idhash in PixIDHASHtoROBIDmap and int(PixIDHASHtoROBIDmap[idhash], 0) != int(matchedModules[0].ROBID, 0):
					print 'Inconsistent ROBIDs found! =>', int(PixIDHASHtoROBIDmap[idhash],0), int(matchedModules[0].ROBID,0)
				else:
					# matched module found
					
					PixIDHASHtoROBIDmap[idhash] = matchedModules[0].ROBID
			elif isSCT == '1':
				if idhash in SCTIDHASHtoROBIDmap and int(SCTIDHASHtoROBIDmap[idhash], 0) != int(matchedModules[0].ROBID, 0):
					print 'Inconsistent ROBIDs found! =>', int(SCTIDHASHtoROBIDmap[idhash],0), int(matchedModules[0].ROBID,0)
				else:
					# matched module found
					
					SCTIDHASHtoROBIDmap[idhash] = matchedModules[0].ROBID

	return (PixIDHASHtoROBIDmap, SCTIDHASHtoROBIDmap)

def checkConsistency(pixmapname, sctmapname, datafilename, moduleconfigfile):
	pixmap = parsePixCablingMap(pixmapname)
	sctmap = parseSCTCablingMap(sctmapname)
	# ROBmap: ROBId -> tower -> [ (modid, layer) ]
	ROBmap = dfconfigparser.parseModuleListFile( moduleconfigfile)[0]
	ROBSofInterest = getInputROBIDS("/afs/cern.ch/user/e/etolley/work/DFDataFlow/config/df/df_multiboard_config_allshelves.txt")

	PixIDHASHtoROBIDmap = {}
	SCTIDHASHtoROBIDmap = {}


	# data file should have line structure
	# with the corrected layer and eta module
	# Tower Event isSCT isPixel BarrelEC Layer PhiModule EtaModule PhiSide IDHash
	file = open(datafilename,'r')
	prev_event = -1

	count_pix_total = 0
	count_sct_total = 0
	count_pix_valid = 0
	count_sct_valid = 0
	count_pix_matched = 0
	count_sct_matched = 0
	count_pix_perfect = 0
	count_sct_perfect = 0
	
	for line in file:
		toks = line.strip().split()
		if len(toks) == 0: continue
		if '#' == toks[0]: continue
		(tower, event, isSCT, isPix, barrel_ec, layer, phi_module, eta_module, phiside, idhash) = toks[:10]
		event = int(event)
		tower = int(tower)
		idhash_int = int(idhash)

		if event > 0: continue

		if event != prev_event:
			print event
			prev_event = event
		
		matched_module = None
		robid = -1
		
		if isPix == '1':
			for module in pixmap:
				if module.barrel_ec == barrel_ec and module.phi_module == phi_module and module.eta_module == eta_module and module.layer == layer:
					matched_module = module
		else:
			for module in sctmap:
				#print module.barrel_ec, barrel_ec, module.barrel_ec == barrel_ec
				if module.barrel_ec == barrel_ec and module.phi_module == phi_module and module.eta_module == eta_module and module.layer == layer:
					matched_module = module

		if matched_module: robid = matched_module.ROBID
		else:
			print '{0} module {1} not found in map!'.format("Pixel" if  isPix == '1' else "SCT" ,idhash)
			break

		if robid not in ROBSofInterest:
			print "Module {0} robid {1} should go to tower {2}, but ROBID not in an input IM lane".format(idhash_int, robid, tower)
			continue

		if isPix == '1':
			count_pix_total += 1
		else:
			count_sct_total += 1

		SCTROB = robid >= 0x200000
		if SCTROB and isPix == '1':
			print 'ERROR! Pixel Module {0} has SCT ROBID {1}'.format(idhash, robid)
			sys.exit(2)
		elif not SCTROB and not isPix == '1':
			print 'ERROR! SCT Module {0} has Pixel ROBID {1}'.format(idhash, robid)
			sys.exit(2)



		if robid not in ROBmap:
			print "Module {0} robid {1} should go to tower {2}, but robid not found in module config!".format(idhash_int, robid, tower)
			print 'possible robid candidates:'
			for cr, modmap in ROBmap.iteritems():
				for ci, towers in ROBmap.iteritems():
					if ci == idhash_int and tower in towers: print cr
			continue

		if isPix == '1': count_pix_valid += 1
		else:            count_sct_valid += 1 
		
		match_found = False
		match_tower = False
		for modid, modtowers in ROBmap[robid].iteritems():
			# found matching module in moduleconfig
			if modid == idhash_int: 
				if matched_module and not SCTROB:
					match_found = True
					if tower in modtowers:
						match_tower = True
				elif matched_module and SCTROB:
					match_found = True
					if tower in modtowers:
						match_tower = True
					#if matched_module.ROBID != robid:
					#	print "SCT", modid, matched_sct_module.ROBID, robid
		if match_found:
			if isPix == '1': count_pix_matched += 1
			else: count_sct_matched += 1 
		else:
			print "Module ID {0} (with robid {1}) not found in module config!".format(idhash_int, robid)
			print 'possible robid candidates:'
			for cr, modmap in ROBmap.iteritems():
				for ci, towers in ROBmap.iteritems():
					if ci == idhash_int and tower in towers: print cr

		if match_tower:
			if isPix == '1': count_pix_perfect += 1
			else: count_sct_perfect += 1 


	print "Total Pixel Modules:", count_pix_total
	print "Total SCT Modules:", count_sct_total
	print "Pixel Modules with ROBID in config:", count_pix_valid
	print "SCT Modules with ROBID in config:", count_sct_valid
	print "Pixel Modules with ROBID & ModuleID in config:", count_pix_matched
	print "SCT Modules with ROBID & ModuleID in config:", count_sct_matched
	print "Pixel Modules with ROBID, ModuleID, & Tower in config:", count_pix_perfect
	print "SCT Modules with ROBID, ModuleID, & Tower in config:", count_sct_perfect





if __name__=='__main__':
	#pixmap = parsePixCablingMap()
	#sctmap = parseSCTCablingMap()
	datafilename = "/afs/cern.ch/user/e/etolley/work/DFDataFlow/data/NTUP_FTKIP_ttbar_words.txt"
	pixmapname = "/afs/cern.ch/user/e/etolley/work/DFDataFlow/config/geometry/Pixels_Atlas_IdMapping_2018.dat"
	sctmapname = "/afs/cern.ch/user/e/etolley/work/DFDataFlow/config/geometry/SCT_Cabling_svc_1565613360.dat"
	moduleconfigfile = "/afs/cern.ch/user/e/etolley/work/DFDataFlow/config/df/modulelist_Data2018_64T.txt"

	print 'Reading Pixel map from', pixmapname
	print 'Reading SCT map from',   sctmapname
	print 'Reading FTK data from',  datafilename

	checkConsistency(pixmapname, sctmapname, datafilename, moduleconfigfile)
	'''
	(PixIDHASHtoROBIDmap,  SCTIDHASHtoROBIDmap)  = buildIDHashROBMap(pixmapname, sctmapname, datafilename)

	pixjsonname = 'PixelIDHashROBMap.json'
	sctjsonname   = 'SCTIDHashROBMap.json'
	print 'Writing', pixjsonname, 'and', sctjsonname
	import json
	with open(pixjsonname, 'w') as outfile:  
		json.dump(PixIDHASHtoROBIDmap, outfile, indent=4)
	with open(sctjsonname, 'w') as outfile:  
		json.dump(SCTIDHASHtoROBIDmap, outfile, indent=4)

	(PixIDHASHtoROBIDmap2, SCTIDHASHtoROBIDmap2) = buildIDHashROBMapfromModuleConfig(moduleconfigfile)

	pixjsonname = 'PixelIDHashROBMap_fromconfig.json'
	sctjsonname   = 'SCTIDHashROBMap_fromconfig.json'
	print 'Writing', pixjsonname, 'and', sctjsonname
	import json
	with open(pixjsonname, 'w') as outfile:  
		json.dump(PixIDHASHtoROBIDmap2, outfile, indent=4)
	with open(sctjsonname, 'w') as outfile:  
		json.dump(SCTIDHASHtoROBIDmap2, outfile, indent=4)'''

	




