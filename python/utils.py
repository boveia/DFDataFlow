import sys
from sets import Set
import math
import logging

def toHex(thisstr):
	return "0x{:02x}".format(thisstr)

def hashROBModID(robid, modid):
	return int(modid * 10e7 + robid)

def unhash(hashval):
	return ( int(hashval%10e7), int(hashval/10e7))
	
def make_str_from_list(x):
	return ''.join([str(s) for s in x])

def number_to_bit_list( x, nbits = 11):
	formatstr = '{:0'+str(nbits)+'b}'
	output = [int(x) for x in formatstr.format(x)]
	return output

def remove_duplicates(x):
	return list(Set(x))

def reduce_identical_list(x):
	if x.count(x[0]) == len(x):
		return x[0]
	else:
		print 'Elements should all be identical!'
		print x
		sys.exit(2)

def mean(x):
	return sum(x)*1.0/len(x)

def rms(x):
	ms = sum([i**2 for i in x])/ len(x)
	return math.sqrt(ms)

def flatten_list(l):
	flat_list = [item for sublist in l for item in sublist]
	return flat_list

def log(instr):
	print instr
	logging.info(instr)

def error(instr):
	print "ERROR: {0}".format(instr)
	logging.error(instr)
	sys.exit(2)
	
def warning(instr):
	print "WARNING: {0}".format(instr)
	logging.warning(instr)
	
def printtime(instr):
	print "\033[1;43;40mTIMING: {0}".format(instr)
	logging.debug("TIMING: {0}".format(instr))
	
def trace(instr):
	print "\033[1;46;40mTRACING: {0}".format(instr)
	logging.info("TRACING: {0}".format(instr))