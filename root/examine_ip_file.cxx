#include <set>
#include <iostream>
#include <fstream>

//ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TString.h"
#include "TError.h" //gives us Info, Error, etc, methods to do printout

// Athena includes
#include "TrigFTKSim/FTKHit.h"
#include "TrigFTKSim/FTKRawHit.h"

ofstream myoutfile;


void examineHit( const FTKRawHit * thisHit, bool verbose = false){
// #Barrel_EC Layer_Disk  Phi_module  Eta_module  ROBID RODID 40FMT 40Link  80FMT 80Link  DCS Geographical ID
            std::cout << "\t" << " isSCT: "  << thisHit->getIsSCT()
                         <<" BarrelEC: " << thisHit->getBarrelEC()
                         << " piside:" << thisHit->getPhiSide()
                         << " layer: "<< thisHit->getLayer() << std::endl;
                         //getPhiModule()
                         //getEtaModule()
}

void writeHit( const FTKRawHit * thisHit, int tower, int event, bool verbose = false){
// #Barrel_EC Layer_Disk  Phi_module  Eta_module  ROBID RODID 40FMT 40Link  80FMT 80Link  DCS Geographical ID
  int layer = thisHit->getLayer();
  std::cout << "\t" 
               << " Tower: " << tower
               << " Event: " << event
               << " isSCT: "  << thisHit->getIsSCT()
               << " isPixel: "  << thisHit->getIsPixel()
               <<" BarrelEC: " << thisHit->getBarrelEC()
               << " layer: "  << layer
               <<" Phi Module: " << thisHit->getPhiModule()
               <<" Eta Module: " << thisHit->getEtaModule()
               << " Phi Side:" << thisHit->getPhiSide()
               << " ID Hash:" << thisHit->getIdentifierHash()
               << std::endl;

  myoutfile << " " << tower
            << " " << event
            << " "  << thisHit->getIsSCT()
            << " "  << thisHit->getIsPixel()
            <<" " << thisHit->getBarrelEC()
            << " "<< layer
            <<" " << thisHit->getPhiModule()
            <<" " << thisHit->getEtaModule()
            << " " << thisHit->getPhiSide()
            << " " << thisHit->getIdentifierHash() << "\n";
}

int examine_ip_file(  ) {

  TString infilename = "/eos/atlas/user/j/jkuechle/public/ftk/ttbar.NTUP_FTKIP.root";
  myoutfile.open ("ipfile_ttbar.txt");

  TFile * infile = new TFile(infilename,"READ");
  TTree * FTKHitTree = (TTree*)infile->Get("ftkhits");


  myoutfile << "# Tower Event isSCT isPixel BarrelEC Layer PhiModule EtaModule PhiSide IDHash\n";

  std::vector<TH1F*> hists_layer_modulecount;
  std::vector<TH1F*> hists_layer_clustercount;
  for (int i = 1; i <= 8; i++){
    TH1F* h_layer_modulecount  = new TH1F(Form("h_layer%d_modulecount",i),  Form("h_layer%d_modulecount",i),   20, 0, 100);
    TH1F* h_layer_clustercount = new TH1F(Form("h_layer%d_clustercount",i), Form("h_layer%d_clustercount",i), 200, 0, 5000);

    h_layer_modulecount->GetXaxis()->SetTitle(Form("# of modules in layer %d", i));
    h_layer_clustercount->GetXaxis()->SetTitle(Form("# of clusters in layer %d", i));

    hists_layer_modulecount.push_back( h_layer_modulecount);
    hists_layer_clustercount.push_back(h_layer_clustercount);
  }

  std::vector<std::vector<FTKRawHit>*> RawHits(64);
  for (int tower = 0; tower < 64; tower++){
    RawHits[tower] = 0;
    auto hitb = FTKHitTree->GetBranch(Form("RawHits%d",tower));
    hitb->SetAddress(&RawHits[tower]);
  }

  for(int event = 0; event<FTKHitTree->GetEntries(); event++) {

    FTKHitTree->GetEntry(event);
    
    // iterate over towers
    for (int tower = 0; tower < 64; tower++){
      if (RawHits[tower]->size() == 0) continue;
      std::set<unsigned int> uniqueIds;
      std::cout << "Tower " << tower << ", # hits: " << RawHits[tower]->size();
      int layer_modulecount  [] = {0,0,0,0,0,0,0,0};
      int layer_clustercount [] = {0,0,0,0,0,0,0,0};

      for (unsigned int  i = 0; i < RawHits[tower]->size(); i++){

        FTKRawHit * thisHit = &((RawHits[tower])->at(i));
        //thisHit->normalizeLayerID();

        unsigned int idHash = thisHit->getIdentifierHash();  

        if (uniqueIds.count(idHash) == 0){
          uniqueIds.insert(idHash);
          writeHit(thisHit, tower, event);
        }

      }
      std::cout << ", # modules:" << uniqueIds.size() << std::endl;
      /*for (int i = 0; i < 8; i++){
        hists_layer_modulecount[i]->Fill(layer_modulecount[i]);
        hists_layer_clustercount[i]->Fill(layer_clustercount[i]);
      }*/
    }
    break;
  }

  myoutfile.close();

  return 0;
 
}

