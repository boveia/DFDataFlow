

import sys,  getopt
from IPParser import parseIPFile, Hit
from sets import Set
import ROOT

'''
Some files:
/eos/atlas/atlascerngroupdisk/det-ftk/tvlibrary/data18_pp_2017LowDF/Tower22_Run358656_100events/IPGen/ftksim_64Towers_wrap.root
/afs/cern.ch/user/h/hartj/public/forTova/tmp.NTUP_FTKIP

/eos/atlas/user/j/jkuechle/public/ftk/ttbar.NTUP_FTKIP.root 
/eos/atlas/user/j/jkuechle/public/ftk/data18_13TeV.00360026.physics_EnhancedBias.NTUP_FTKIP.root 
'''

def processTowertoHitMap(TowertoHitMap, hists):
	count_towers = 0
	count_pixelhits = 0
	count_iblhits = 0
	count_scthits = 0
	for tower, hits in TowertoHitMap.iteritems():
		if len(hits) == 0: continue
		uniqueHitIDs = Set([h.idhash for h in hits])
		print "Tower %s sees %s hits and %s modules"%(tower,len(hits), len(uniqueHitIDs))

		n_pixelhits = sum([h.isPIXEL() for h in hits])
		n_iblhits   = sum([h.isIBL()   for h in hits])
		n_scthits   = sum([h.isSCT()   for h in hits])

		uniqueHits = []
		for h in hits:
			if h.idhash in uniqueHitIDs:
				uniqueHitIDs.remove(h.idhash)
				uniqueHits.append(h)
		n_pixelmodules = sum([h.isPIXEL() for h in uniqueHits])
		n_iblmodules   = sum([h.isIBL()   for h in uniqueHits])
		n_sctmodules   = sum([h.isSCT()   for h in uniqueHits])

		count_towers += 1
		count_pixelhits += n_pixelmodules
		count_iblhits += n_iblmodules
		count_scthits += n_sctmodules

		hists['h_pixel_modules'].Fill(tower,n_pixelmodules)
		hists['h_sct_modules'].Fill(tower,n_sctmodules)
		hists['h_ibl_modules'].Fill(tower,n_iblmodules)
		hists['h_pixel_hits'].Fill(tower,n_pixelhits)
		hists['h_sct_hits'].Fill(tower,n_scthits)
		hists['h_ibl_hits'].Fill(tower,n_iblhits)

	print "\n%s towers saw hits"%count_towers
	print "Average # of Pixel modules = %s"%(count_pixelhits*1.0/count_towers)
	print "Average # of SCT modules = %s"%(count_scthits*1.0/count_towers)
	print "Average # of IBL modules = %s"%(count_iblhits*1.0/count_towers)

def main(argv):

	#===== Arguent parsing =====#	
	inputfilename = ''
	try:
		opts, args = getopt.getopt(argv,'hi:',['file='])
	except getopt.GetoptError:
		print "examine_ipfile.py -i <inputfilename> "
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print "Usage: examine_ipfile.py -i <inputfilename>"
			sys.exit()
		elif opt in ('-i', '--ifile'):
			inputfilename = arg
	if inputfilename == '':
		print "\nUsing default input file"
		print "Define alternate input file with:"
		print "examine_ipfile.py -i <inputfilename> "
		inputfilename = "ipfiles/ftksim_64Towers_wrap.root"
	print "Input file is %s\n"%inputfilename

	#===== Get IP file data =====#	
	TowertoHitMapList = parseIPFile(inputfilename)

	#===== define hists =====#	
	hists = {
	'h_pixel_modules':  ROOT.TH1F("h_pixel_modules",  "h_pixel_modules",   64,-1,63),
	'h_ibl_modules':    ROOT.TH1F("h_ibl_modules",    "h_ibl_modules",     64,-1,63),
	'h_sct_modules':    ROOT.TH1F("h_sct_modules",    "h_sct_modules",     64,-1,63),
	'h_pixel_hits':  ROOT.TH1F("h_pixel_hits",  "h_pixel_hits",   64,-1,63),
	'h_ibl_hits':    ROOT.TH1F("h_ibl_hits",    "h_ibl_hits",     64,-1,63),
	'h_sct_hits':    ROOT.TH1F("h_sct_hits",    "h_sct_hits",     64,-1,63),
	'h_barrel': ROOT.TH1F("h_barrel", "h_barrel",  64,-1,63),
	'h_endcap': ROOT.TH1F("h_endcap", "h_endcap",  64,-1,63),
	}

	#===== Go through tower--> hit map =====#
	for TowertoHitMap in TowertoHitMapList:
		processTowertoHitMap(TowertoHitMap, hists)

	#===== Plotting =====#
	ROOT.gStyle.SetLegendFillColor(0)
	ROOT.gStyle.SetLegendBorderSize(0)
	ROOT.gStyle.SetOptTitle(0)
	ROOT.gStyle.SetOptStat(0)

	# hist formatting
	sf = 1./len(TowertoHitMapList)
	for name, h in hists.iteritems():
		h.Scale(sf)
		h.SetLineColor(ROOT.kBlack)
		for t in range(64): h.GetXaxis().SetBinLabel(t+1,"%s"%(t));

	hists['h_pixel_modules'].SetFillColor(ROOT.kRed-4)
	hists['h_ibl_modules'].SetFillColor(ROOT.kOrange+1)
	hists['h_sct_modules'].SetFillColor(ROOT.kAzure+1)
	hists['h_pixel_hits'].SetFillColor(ROOT.kPink-2)
	hists['h_ibl_hits'].SetFillColor(ROOT.kOrange-2)
	hists['h_sct_hits'].SetFillColor(ROOT.kBlue-7)

	# canvas setup
	canvas = ROOT.TCanvas("c1","c1",1000,600)
	legend1 = ROOT.TLegend(0.65,0.6,0.89,0.8)
	legend2 = ROOT.TLegend(0.65,0.6,0.89,0.8)
	pad1 = ROOT.TPad("pad1","This is pad1",0.0,0.51,1.0,0.98);
  	pad2 = ROOT.TPad("pad2","This is pad2",0.0,0.01,1.0,0.48);
  	pad1.SetGridy()
  	pad2.SetGridy()
	pad1.Draw()
	pad2.Draw()

	pad1.cd()

	hs1 = ROOT.THStack("hs1","")
	legend1.AddEntry(hists['h_pixel_modules'],"Pixel Modules",'f')
	legend1.AddEntry(hists['h_ibl_modules'],"IBL Modules",'f')
	legend1.AddEntry(hists['h_sct_modules'],"SCT Modules",'f')
	hs1.Add(hists['h_ibl_modules'])
	hs1.Add(hists['h_pixel_modules'])
	hs1.Add(hists['h_sct_modules'])
	hs1.Draw("hist")
	hs1.GetXaxis().SetTitle("Tower #")
	hs1.GetXaxis().SetLabelSize(0.07)
	hs1.GetXaxis().SetTitleSize(0.07)
	hs1.GetYaxis().SetLabelSize(0.06)
	hs1.GetYaxis().SetTitleSize(0.06)
	#hs1.GetXaxis().SetTitleOffset(1.1)
	hs1.GetYaxis().SetTitle("Average # of modules per event")
	legend1.Draw()

	pad2.cd()

	hs2 = ROOT.THStack("hs1","")
	legend2.AddEntry(hists['h_pixel_hits'],"Pixel Clusters",'f')
	legend2.AddEntry(hists['h_ibl_hits'],"IBL Clusters",'f')
	legend2.AddEntry(hists['h_sct_hits'],"SCT Clusters",'f')
	hs2.Add(hists['h_ibl_hits'])
	hs2.Add(hists['h_pixel_hits'])
	hs2.Add(hists['h_sct_hits'])
	hs2.Draw("hist")
	hs2.GetXaxis().SetTitle("Tower #")
	hs2.GetXaxis().SetLabelSize(0.07)
	hs2.GetXaxis().SetTitleSize(0.07)
	hs2.GetYaxis().SetLabelSize(0.06)
	hs2.GetYaxis().SetTitleSize(0.06)
	#hs2.GetXaxis().SetTitleOffset(1.1)
	hs2.GetYaxis().SetTitle("Average # of clusters per event")
	legend2.Draw()

	canvas.cd()
	canvas.SaveAs("examine_ipfile.png")
	canvas.SaveAs("examine_ipfile.C")

	raw_input("Press enter...")

if __name__=='__main__':
	main(sys.argv[1:])

		
