import ROOT
import ast
import sys
from sets import Set

from collections import namedtuple

Module = namedtuple("Module","type id robid in_df imchan layer avgwords rmswords out_dfs")

shelf_1 = range(0,30,4)
shelf_2 = [i+1 for i in range(0,30,4)]
shelf_3 = [i+2 for i in range(0,30,4)]
shelf_4 = [i+3 for i in range(0,30,4)]

all_boards = shelf_1 + shelf_2 + shelf_3 + shelf_4

norm_factor = [0] * (len(all_boards)+1)

normalize = False

def loadModuleMap(infilename, normalize = False):
	hist = ROOT.TH2D("h","h",len(all_boards ), 0, 32, len(all_boards), 0, 32)
	for i in range(len(all_boards)):
		hist.GetXaxis().SetBinLabel(i+1,"{0}".format(all_boards[i]));
		hist.GetYaxis().SetBinLabel(i+1,"{0}".format(all_boards[i]));
	hist.LabelsOption("v", "X")
	hist.GetXaxis().SetTitle("In DF")
	hist.GetYaxis().SetTitle("Out DF")
	if normalize:
		hist.GetZaxis().SetTitle("% of words/event received from IM by each DF")
	else:
		hist.GetZaxis().SetTitle("average # of words/event")
	hist.GetZaxis().SetTitleOffset(1.5)
	infile = open(infilename, 'r')
	modules_of_interest = []
	for line in infile:
		#print line, line[0],  line[0] == '#'
		if line.strip()[0] == '#': continue
		#"# Type;", "Mod. #;","ROBID;","In DF #;","In IM chan.;","Layer;","Mean words;","RMS Words;","Destination DFs;","Penultimate DFs;","Destination Towers;"
		toks = [t.strip() for t in line.split(';')]
		try:
			(mtype, mid, robid, in_df, imchan, layer, avgwords, rmswords, out_dfs, pendfs, desttows, moutput, __) = toks
			in_df = int(in_df)
			out_dfs = ast.literal_eval(out_dfs)
			avgwords = float(avgwords)
		except:
			print "Line not expected: {0}".format(toks)
			print "Check input file {0}".format(infilename)
			sys.exit(2)

		for out_df in out_dfs:
			if in_df == 29 or in_df == 30 or in_df == 15 or in_df == 19: # set filter # 29,30,15 and 19
				modules_of_interest.append(Module(int(mtype), int(mid), int(robid), in_df, int(imchan), layer, avgwords, rmswords, out_dfs))
			#hist.SetBinContent(all_boards.index(in_df), all_boards.index(out_df), mean_words)
			hist.Fill(all_boards.index(in_df), all_boards.index(out_df), avgwords)
			norm_factor[all_boards.index(in_df)] += avgwords

	if normalize:
		for i in range(len(all_boards)):
			print "Board {0} received {1} words/event".format(all_boards[i], norm_factor[i])
			for j in range(len(all_boards)):
				prctwords = hist.GetBinContent(i+1, j+1)/norm_factor[i]*100.
				print "{2}% of words went from {0}->{1}".format(all_boards[i],all_boards[j], prctwords)
				hist.SetBinContent(i+1, j+1,prctwords)

	# figure out boards with highest rate
	for i in range(len(all_boards)):
		for j in range(len(all_boards)):
			if i == j: continue
			in_df = all_boards[i]
			out_df = all_boards[j]
			avgwords = hist.GetBinContent(i+1, j+1)
			if avgwords > 3000:
				print "Board {0}->{1}: {2} words/event".format(in_df, out_df, avgwords)

	return (hist, modules_of_interest)



indir = "/afs/cern.ch/user/e/etolley/work/DFDataFlow/output/"
fixedmap  = "DataFlow_NTUP_FTKIP_ttbar_words_30evts_32boards_fixedMap/moduleSummary.txt"
randommap = "DataFlow_NTUP_FTKIP_ttbar_words_20evts_32boards_randomMap/moduleSummary.txt"

#_fixedMap
#infilename = "{0}/DataFlow_NTUP_FTKIP_ttbar_words_10evts_32boards_moduleSummary.txt".format(indir)
#outfilename = "randommap.root"


(histfixed,  modules)  = loadModuleMap("{0}/{1}".format(indir, fixedmap ), normalize = False)
#(histrandom, modulesrandom) = loadModuleMap("{0}/{1}".format(indir, randommap),  normalize = True)
'''
problem_boards = [29, 30, 15, 19]
outfiles = []
for b in problem_boards:
	f = open("moduleStats_df{0}.txt".format(b), 'w')
	outfiles.append(f)

problem_robids = {29:{}, 30:{}, 15:{}, 19:{}}
for m in modules:
	for i, b in enumerate(problem_boards):
		if m.in_df == b:
			outfiles[i].write("{0}\n".format(m))
			try:    problem_robids[b][m.robid][1] += 1
			except: problem_robids[b][m.robid] = [0,1]
			if b not in m.out_dfs:
				problem_robids[b][m.robid][0] += 1

for f in outfiles: f.close()

for b, robmap in problem_robids.iteritems():
	print "Board {0}".format(b)
	for r, mods in robmap.iteritems():
		print "ROBID {0} has {1}/{2} modules which do not go to board {3}".format(r, mods[0], mods[1],b)
'''

#===== Plotting =====#
ROOT.gStyle.SetLegendFillColor(0)
ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gStyle.SetOptTitle(0)
ROOT.gStyle.SetOptStat(0)
canvas = ROOT.TCanvas('c','c', 650, 600)
canvas.SetRightMargin(0.2)
canvas.SetTopMargin(0.05)
#canvas.SetLogz()
histfixed.Draw("colz")
#histr.GetZaxis().SetRangeUser(-10,10)

#outfile = ROOT.TFile(outfilename,"RECREATE")
#histr.Write()
#outfile.Close()

canvas.SaveAs("moduleDataFlow.png")

raw_input("Wait...")