 

import sys,  getopt, os
import ROOT

def main(argv):

	thistower = 10

	#===== define hists =====#	
	hists = {}
	for i in range(9):
		hists['h_SCTlayer{0}'.format(i)] =  ROOT.TH1F('h_SCTlayer{0}'.format(i),  'h_SCTlayer{0}'.format(i),  50, 0, 2000)
		hists['h_SCTlayer{0}'.format(i)].GetXaxis().SetTitle('# words/event')
		hists['h_SCTlayer{0}'.format(i)].GetXaxis().SetTitleSize(0.08)
	for i in range(4):
		hists['h_PIXlayer{0}'.format(i)] =  ROOT.TH1F('h_PIXlayer{0}'.format(i),  'h_PIXlayer{0}'.format(i),  50, 0, 1000)
		hists['h_PIXlayer{0}'.format(i)].GetXaxis().SetTitle('# words/event')
		hists['h_PIXlayer{0}'.format(i)].GetXaxis().SetTitleSize(0.08)

	#===== loop over root file =====#

	workdir = os.environ['TestArea']
	outdir = "{0}/plotting/".format(workdir)
	plot_exts = ['.png']

	infilename = "{0}/data/NTUP_FTKIP_ttbar_words.root".format(workdir)
	outTag = "ttbar"
	plottag ="ttbar"
	#infilename = "{0}/data/NTUP_FTKIP_data18_13TeV.00360026.physics_EnhancedBias_words.root".format(workdir)
	#outTag = "data18"
	#plottag = "data18 enhanced bias"
	
	inputfile = ROOT.TFile(infilename, 'READ') 
	ftkhits = inputfile.Get("ftkhits")
	maxEvts = ftkhits.GetEntries()
	for evt in range(0, maxEvts):
		print 'Event', evt
		ftkhits.GetEntry(evt)
		towercount = {}
		t = thistower
		for l in range(9):
			towercount["{0}sct{1}".format(t,l)] = 0
		for l in range(4):
			towercount["{0}pix{1}".format(t,l)] = 0

		for i in range(len(ftkhits.tower)):
			tower = ftkhits.tower[i]
			if tower!= thistower: continue

			isPixel = ftkhits.isPixel[i]
			isSCT = ftkhits.isSCT[i]
			layer = ftkhits.layer[i]
			moduleSize = ftkhits.moduleSize[i]
			nwords = ftkhits.moduleClusters[i]

			# convert to FTK plane
			if isSCT and ftkhits.BEC[i] == 0:
				piSide = ftkhits.phiSide[i]
				if   layer == 0:
					if   piSide == 1: layer = 0
					elif piSide == 0: layer = 1
				elif layer == 1:
					if   piSide == 0: layer = 2
					elif piSide == 1: layer = 3
				elif layer == 2:
					if   piSide == 1: layer = 4
					elif piSide == 0: layer = 5
				elif layer == 3:
					if   piSide == 0: layer = 6
					elif piSide == 1: layer = 7
 
			if   isSCT:   towercount["{0}sct{1}".format(tower,layer)] += nwords #hists['h_SCTlayer{0}'.format(layer)].Fill(nwords)
			elif isPixel: towercount["{0}pix{1}".format(tower,layer)] += nwords #hists['h_PIXlayer{0}'.format(layer)].Fill(nwords)

		for l in range(9):
			hists['h_SCTlayer{0}'.format(l)].Fill(towercount["{0}sct{1}".format(t,l)])
		for l in range(4):
			hists['h_PIXlayer{0}'.format(l)].Fill(towercount["{0}pix{1}".format(t,l)])

	#===== Plotting =====#
	ROOT.gStyle.SetLegendFillColor(0)
	ROOT.gStyle.SetLegendBorderSize(0)
	ROOT.gStyle.SetOptTitle(0)
	ROOT.gStyle.SetOptStat(0)

	# hist formatting
	colors = [ROOT.kPink+9, ROOT.kRed-4, ROOT.kOrange+1,ROOT.kYellow, ROOT.kTeal+1, ROOT.kAzure+1, ROOT.kBlue-7, ROOT.kViolet]

	for i in range(9):
		#hists['h_SCTlayer{0}'.format(i)].Scale(sf)
		hists['h_SCTlayer{0}'.format(i)].SetLineColor(ROOT.kBlack)
		hists['h_SCTlayer{0}'.format(i)].SetFillColor(colors[i%len(colors)])
	for i in range(4):
		hists['h_PIXlayer{0}'.format(i)].SetLineColor(ROOT.kBlack)
		hists['h_PIXlayer{0}'.format(i)].SetFillColor(colors[i%len(colors)])

	# canvas setup
	canvas1 = ROOT.TCanvas("c1","c1",1000,600)
	canvas2 = ROOT.TCanvas("c2","c2",800,300)



	column1 = []
	for i in range(5):
		column1.append(ROOT.TPad("pada{0}".format(i+5),
			                     "This is pad{0}".format(i+5),
			                      i/5., 0.5, i/5. + 0.2, 1.0))
	for i in range(4):
		column1.append(ROOT.TPad("pada{0}".format(i),
			                     "This is pad{0}".format(i),
			                      i/5., 0.0, i/5. + 0.2, 0.5))


	column2 = []
	for i in range(4):
		column2.append(ROOT.TPad("padb{0}".format(i),
			                     "This is pad{0}".format(i),
			                     i/4., 0.0, i/4. + 0.25, 1.0))

	pads = column1 + column2
	for p in pads:
		p.SetTopMargin(0.02)
		p.SetBottomMargin(0.2)
		p.SetGridy()

	mytext =    ROOT.TLatex()
	mytext.SetTextSize(0.07)
	mytext.SetNDC()

	canvas1.cd()
	for p in column1: p.Draw()
	for i in range(len(column1)):
		p.Draw()
		column1[i].cd()
		hists['h_SCTlayer{0}'.format(i)].SetMaximum(hists['h_SCTlayer{0}'.format(i)].GetMaximum()*1.5)
		hists['h_SCTlayer{0}'.format(i)].Draw('hist')
		avg = hists['h_SCTlayer{0}'.format(i)].GetMean()
		yt = 0.9
		mytext.DrawLatex(.2,yt,plottag)
		mytext.DrawLatex(.2,yt - 0.07,"Tower {0}".format(thistower))
		mytext.DrawLatex(.2,yt - 0.07*2,"SCT Layer {0}".format(i))
		mytext.DrawLatex(.2,yt - 0.07*3,"Average = {0}".format(avg))
		canvas1.cd()
	canvas1.cd()
	#canvas1.Draw()
	#canvas1.Modified()
	#canvas1.Update()

	plotname = "{2}/plotting/SCTlayers_tower{0}_{1}plot.png".format(thistower,outTag,outdir)
	canvas1.SaveAs(plotname)


	canvas2.cd()
	for p in column2: p.Draw()
	for i in range(len(column2)):
		column2[i].cd()
		hists['h_PIXlayer{0}'.format(i)].Draw('hist')
		hists['h_PIXlayer{0}'.format(i)].SetMaximum(hists['h_PIXlayer{0}'.format(i)].GetMaximum()*1.5)
		avg = hists['h_PIXlayer{0}'.format(i)].GetMean()
		yt = 0.9
		mytext.DrawLatex(.2,yt,plottag)
		mytext.DrawLatex(.2,yt-0.07,"Tower {0}".format(thistower))
		mytext.DrawLatex(.2,yt-0.07*2,"Pixel Layer {0}".format(i))
		mytext.DrawLatex(.2,yt-0.07*3,"Average = {0}".format(avg))
		canvas2.cd()
	canvas2.cd()

	plotname = "{2}/Pixlayers_tower{0}_{1}plot.png".format(thistower,outTag,outdir)
	canvas2.SaveAs(plotname)

	raw_input("Press enter...")

if __name__=='__main__':
	main(sys.argv[1:])

		
