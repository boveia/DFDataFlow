import ROOT

inputfile = ROOT.TFile("data/NTUP_FTKIP_ttbar_words.root", 'READ')
ftkhits = inputfile.Get("ftkhits")

maxEvts = 5 # ftkhits.GetEntries()

my2d_num = ROOT.TH2D("h1","",maxEvts,0,maxEvts,64,0,64)
my2d_den = ROOT.TH2D("h1","",maxEvts,0,maxEvts,64,0,64)


for evt in range(0, maxEvts):

	print 'Event', evt

	# get event info from im
	ftkhits.GetEntry(evt)
	#for tower in range (64):
	events    = ftkhits.event
	isSCTs    = ftkhits.isSCT
	isPixels  = ftkhits.isPixel
	idHashes  = ftkhits.IDHash
	idHashes  = ftkhits.moduleSize

	for i in range(len(ftkhits.tower)):
		
		tower = ftkhits.tower[i]
		isPixel = ftkhits.isPixel[i]
		layer = ftkhits.layer[i]

		if layer!= 1: continue
		if not isPixel: continue

		my2d_den.Fill(evt, tower, 1)
		my2d_num.Fill(evt, tower, ftkhits.moduleSize[i])

ROOT.gStyle.SetOptStat(0)

my2d_num.GetXaxis().SetTitle("Event #")
my2d_num.GetYaxis().SetTitle("Tower #")
my2d_num.GetZaxis().SetTitle("Module Size [bits]")
#my2d_num.Divide(my2d_den)
my2d_num.Draw("colz")
ROOT.gPad.SetRightMargin(0.15)
raw_input("wait...")